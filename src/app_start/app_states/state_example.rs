use super::AppData;



//: 一个信息例子 
pub struct Example(pub &'static str);

impl Default for AppData<Example> {
    fn default() -> Self {
        AppData::new(Example("大家好,我是训练两年半的实习生,菜虚坤,给大家来一段'鸡尼太美',希望大家喜欢"))
    }
}