use core::time;
use std::{
    cell::{Cell, RefCell},
    sync::{atomic::AtomicUsize, Arc, Mutex}, rc::Rc,
};

// 应用服务 的一些操作
use super::{app_states::AppState};
use crate::tool_bag::FREE_CONFIG;
use actix_web::{middleware::{self, Logger, Condition}, dev::{ServiceFactory, ServiceRequest, ServiceResponse}, Error, body::MessageBody};
use actix_web::{
    dev::Service as _,
    dev::Server,
    get, post,
    web::{self, route},
    App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use env_logger::Env;
// use tracing_actix_web::TracingLogger;

///: 初始化 服务
pub async fn init_server() {
    let free_config = *FREE_CONFIG;

    let app_state = Arc::new(AppState::default());
    

    let mut server = HttpServer::new(move || {
        // ! 注意,闭包内部的一切,都会在不同的工作线程中独立
        // ! 所以 工作线程中共享的状态需要在外部声明后传入
        // ! 注意 此闭包的是 !全局范围! 的,而非web::scope("***")
        
        //: new
        let mut app = App::new();
        //: 挂载全局共享状态
        app = mount_state(app, app_state.clone());
        //: 加载配置,局部(范围)需要在这里面配置
        app = app.configure(app_config);

		//: 挂载全局中间件
        // ! 最后挂载最先执行
		let mut app = app
                .wrap(Condition::new( free_config.log.app_server_Logger_log,Logger::new(free_config.log.app_server_Logger_format.as_str())))
                ;

        app
    });
    //: 加载 workers配置
    if let Some(v) = free_config.app.workers {
        server = server.workers(v);
    }
    //: 配置地址和端口
    let server = server
        .bind((free_config.app.address.clone(), free_config.app.port))
        .unwrap();

    //: 发射!! ~~~ 哦,搞错了,这不是Rocket
    server.run().await;
}

//: 应用配置
//: 这里可以配置:[范围,控制器,守卫,中间件,...]
fn app_config(cfg: &mut web::ServiceConfig) {
    cfg.configure(crate::controller::controller_config);
}

//: 挂载全局共享状态
fn mount_state<T>(app:App<T>,state:Arc<AppState>)->App<T>
where T: ServiceFactory<ServiceRequest, Config = (), Error = Error, InitError = ()>{
	app
		.app_data(state.clone())//: 挂载总状态
		.app_data(state.example.clone())
        .app_data(state.email_code.clone())
}




