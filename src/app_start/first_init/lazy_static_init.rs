// 应用启动时 初始化必要的 静态变量

/// 优先初始化,在任何程序前 初始化
pub async fn init() {
    tokio::task::spawn_blocking(|| {
        //: 初始化 free_config.toml 配置文件
        lazy_static::initialize(&crate::tool_bag::config::free_config::FREE_CONFIG);
        //: 初始化 MySQL
        lazy_static::initialize(&crate::tool_bag::config::mysql::MYSQL_DB);
        //: 初始化 api控制配置对象
        lazy_static::initialize(&crate::tool_bag::config::api_control::API_CONTROL);
        
    }).await.expect("优先初始化错误");
}
