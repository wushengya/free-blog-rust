use std::fs::DirBuilder;

// 其他无法分类的初始化
use crate::tool_bag::FREE_CONFIG;
use env_logger::Env;

pub async fn init() {
    init_path().await;
    init_daily_record().await;
}

/// 初始化日志
async fn init_daily_record() {
    env_logger::init_from_env(Env::default().default_filter_or("info"));
}

/// 初始化路径,准确说应该是文件夹
async fn init_path() {
    let config = &FREE_CONFIG.path;
    let mut dir = DirBuilder::new();
	let dir = dir.recursive(true);

    dir.create(config.root.clone());
    dir.create(config.cent_piece.clone());
    dir.create(config.tem.clone());
}
