// 在这里声明 应用 共享状态
pub mod state_example;
pub mod state_verification_code;
use actix_web::web;
use std::ops::Deref;


//:包装 web::Data
pub struct AppData<T:?Sized>(web::Data<T>);
impl<T> AppData<T> {
	fn new(t:T) -> Self{
		Self(web::Data::new(t))
	}
}
impl<T> Deref for AppData<T> {
    type Target=web::Data<T>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}



//: 在这里声明所有状态
#[derive(Default)]
pub struct AppState {
	pub example: AppData<state_example::Example>,
	pub email_code : AppData<state_verification_code::MVerCodeEmail>
}





