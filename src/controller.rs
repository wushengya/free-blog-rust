use actix_web::web;

pub mod api_test;
pub mod user;


pub fn controller_config(cfg: &mut web::ServiceConfig) {
	cfg.configure(api_test::controller_config);
	cfg.configure(user::controller_config);
	
}
