/// 控制API的开关


use std::{future::{ready, Ready}, borrow::BorrowMut};
use actix_web::{
    dev::{forward_ready, Service, ServiceRequest, ServiceResponse, Transform, Url},
    Error,
};
use futures::executor::block_on;
use futures_util::future::LocalBoxFuture;

pub struct AipRecord;


impl<S> Transform<S, ServiceRequest> for AipRecord
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error>,
    S::Future: 'static,
{
    type Response = ServiceResponse;
    type Error = Error;
    type InitError = ();
    type Transform = ApiRecordMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(ApiRecordMiddleware { service }))
    }
}

pub struct ApiRecordMiddleware<S> {
    service: S,
}

impl<S> Service<ServiceRequest> for ApiRecordMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error>,
    S::Future: 'static,
{
    type Response = ServiceResponse;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    forward_ready!(service);

    // !! 后期完善,未完善,不用使用
    fn call(&self, mut req: ServiceRequest) -> Self::Future {
        todo!();
		let api_type = "Rust";
		let path = req.path().to_string();
		let req_method = req.method();
		let req_method = match req_method.as_str() {
			"GET" | "POST" => req_method.to_string(),
			_ => "OTHER".to_string() //other
		};
		let mut whether_pass = false;//如果表里没有数据,则不通过,因为配置数据,不会加载不通过的数据
        
		let config = block_on(crate::tool_bag::config::api_control::API_CONTROL.read());
        if let Some(v) = config.get(&(req_method,path)){
            whether_pass = *v;
        }
        
        // 它应该是比较 配置对象 ,而非获取表数据比较
		// let url = Url::;
        // req.match_info_mut().set(Url::);
        println!("{:#?}",req.match_info_mut());
		if whether_pass {
            
		}else {
			
		}
        
        let fut = self.service.call(req);
		Box::pin(async move {Ok(fut.await?)})
    }
}