// 配置mysql的连接

use std::time::Duration;
use sea_orm::{DatabaseConnection, ConnectOptions, Database};
use crate::tool_bag::FREE_CONFIG;


lazy_static! {
    static ref db: DatabaseConnection = {
        let config_mysql = &FREE_CONFIG.mysql;
        let mut opt = ConnectOptions::new(format!(
            "mysql://{user}:{password}@{url}",
            user = config_mysql.user,
            password = config_mysql.password,
            url = config_mysql.url
        ));
        opt.max_connections(config_mysql.config.max_connections as u32)
            .min_connections(config_mysql.config.min_connections as u32)
            .connect_timeout(Duration::from_secs(config_mysql.config.connect_timeout))
            .idle_timeout(Duration::from_secs(config_mysql.config.idle_timeout))
            .max_lifetime(Duration::from_secs(config_mysql.config.max_lifetime))
            .sqlx_logging(config_mysql.config.sqlx_log);
        tokio::runtime::Runtime::new().unwrap()
            .block_on(Database::connect(opt))
            .expect("数据库连接错误")
    };
	pub static ref MYSQL_DB:&'static DatabaseConnection = {
		&*db
	};
}


