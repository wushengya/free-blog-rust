mod qq_email;

use lettre::{message::{IntoBody, Mailbox}, transport::smtp::{authentication::Credentials, response::Response, Error}, SmtpTransport, Address};





pub enum EmailBody<T:IntoBody> {
	Text(T),
	Html(T),
	Null
}


pub trait EmailBuild<T:IntoBody>:EmailSend {
	///: 返回证书
	fn get_credentials() -> Credentials;
	///: 返回 smtp 服务器
	fn get_smtp_servers() -> &'static str;
	///: 返回 邮箱
	/// s : 换掉默认名
	fn get_mailbox(s: Option<impl Into<String>>) -> Mailbox ;
	///: 返回 smtp 通讯
	fn get_smtp_transport() -> SmtpTransport {
		SmtpTransport::relay(Self::get_smtp_servers())
			.expect("可能是邮件配置出现问题")
			.credentials(Self::get_credentials()).build()
	}
	///: 返回 @后的字符串 , 123@xxx
	fn get_at_behind()-> &'static str;
	///: 构造一个对象
	fn build() -> Self;
	///: 设置 发送方(我) 的名字
	///: 它会覆盖默认值
	fn from_name(self,s:impl Into<String>) -> Self;
	//# 不打算加入name,而是直接输入 email地址
	//# (user,domain) ("123","qq.com") == 123@qq.com
	///: 设置收件人
	fn to(self,to:Vec<impl Into<Address>>) -> Self;
	///: 设置抄送
	fn cc(self,cc:Vec<impl Into<Address>>) -> Self;
	///: 设置密件抄送
	fn bcc(self,bcc:Vec<impl Into<Address>>) -> Self;
	///: 设置主题
	fn subject( self,subject:impl Into<String>)->Self;
	///: 设置body
	fn body( self,body:EmailBody<T>) -> Self;
}
pub trait EmailSend {

	fn send(self)->Result<Response, Error>;

}


//: 邮件统一使用它来发送
//: 若需要切换邮箱服务,可以直接修改内部的代码
pub struct Email;

impl Email {

	pub fn build<T:IntoBody>() -> impl EmailBuild<T> + EmailSend {
		qq_email::QQEmail::build()
	}
}


