
// !! 需要注意,表的列,对bool类型转换存在问题,会转换为 u8 或 i8 ,需要改成 bool

pub mod prelude;
pub mod sea_orm_active_enums;

#[path ="./table/api_精_准_控_制.rs"]
pub mod api_精_准_控_制;
#[path ="./table/功_能_控_制.rs"]
pub mod 功_能_控_制;
#[path ="./table/博_主_信_息_表.rs"]
pub mod 博_主_信_息_表;
#[path ="./table/博_主_内_容_表.rs"]
pub mod 博_主_内_容_表;
#[path ="./table/博_主_小_标_签_表.rs"]
pub mod 博_主_小_标_签_表;
#[path ="./table/博_主_权_限_表.rs"]
pub mod 博_主_权_限_表;
#[path ="./table/博_主_标_签_锁_表.rs"]
pub mod 博_主_标_签_锁_表;
#[path ="./table/用_户_帐_号_信_息_表.rs"]
pub mod 用_户_帐_号_信_息_表;
#[path ="./table/用_户_权_限_修_改_记_录_表.rs"]
pub mod 用_户_权_限_修_改_记_录_表;
#[path ="./table/用_户_权_限_表.rs"]
pub mod 用_户_权_限_表;
#[path ="./table/用_户_消_息_通_知_队_列_表.rs"]
pub mod 用_户_消_息_通_知_队_列_表;
#[path ="./table/用_户_点_赞_表.rs"]
pub mod 用_户_点_赞_表;
#[path ="./table/用_户_自_定_义_标_签_标_记_内_容_表.rs"]
pub mod 用_户_自_定_义_标_签_标_记_内_容_表;
#[path ="./table/用_户_自_定_义_标_签_表.rs"]
pub mod 用_户_自_定_义_标_签_表;
#[path ="./table/用_户_评_论_表.rs"]
pub mod 用_户_评_论_表;
#[path ="./table/用_户_邮_箱_订_阅_表.rs"]
pub mod 用_户_邮_箱_订_阅_表;
#[path ="./table/管_理_员_权_限_表.rs"]
pub mod 管_理_员_权_限_表;
#[path ="./table/超_级_管_理_员_权_限_表.rs"]
pub mod 超_级_管_理_员_权_限_表;
#[path ="./table/页_面_表.rs"]
pub mod 页_面_表;
#[path ="./table/页_面_观_察_记_录_表.rs"]
pub mod 页_面_观_察_记_录_表;
