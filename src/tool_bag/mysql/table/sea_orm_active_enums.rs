//! SeaORM Entity. Generated by sea-orm-codegen 0.7.0

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "Enum", enum_name = "API类型")]
pub enum Api类型 {
    #[sea_orm(string_value = "Rust")]
    Rust,
    #[sea_orm(string_value = "Java")]
    Java,
    #[sea_orm(string_value = "Vue")]
    Vue,
}
#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "Enum", enum_name = "请求方法")]
pub enum 请求方法 {
    #[sea_orm(string_value = "GET")]
    Get,
    #[sea_orm(string_value = "POST")]
    Post,
    #[sea_orm(string_value = "OTHER")]
    Other,
}
#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "Enum", enum_name = "展示标签")]
pub enum 展示标签 {
    #[sea_orm(string_value = "FutureText_1")]
    FutureText1,
}
#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "Enum", enum_name = "权限")]
pub enum 权限 {
    #[sea_orm(string_value = "用户")]
    用户,
    #[sea_orm(string_value = "博主")]
    博主,
    #[sea_orm(string_value = "管理员")]
    管理员,
    #[sea_orm(string_value = "超级管理员")]
    超级管理员,
}
#[derive(Debug, Clone, PartialEq, EnumIter, DeriveActiveEnum, Serialize, Deserialize)]
#[sea_orm(rs_type = "String", db_type = "Enum", enum_name = "消息类型")]
pub enum 消息类型 {
    #[sea_orm(string_value = "被点赞")]
    被点赞,
    #[sea_orm(string_value = "被评论")]
    被评论,
    #[sea_orm(string_value = "权限被修改")]
    权限被修改,
}
