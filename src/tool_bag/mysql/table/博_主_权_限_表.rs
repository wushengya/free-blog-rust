//! SeaORM Entity. Generated by sea-orm-codegen 0.7.0

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "博主权限表")]
pub struct Model {
    #[sea_orm(column_name = "用户id", primary_key)]
    pub 用_户_id: u32,
    #[sea_orm(
        column_name = "修改下级权限_权限",
        column_type = "Custom(\"SET ('登录_权限\\'评论_权限')\".to_owned())",
        nullable
    )]
    pub 修_改_下_级_权_限_权_限: Option<String>,
    #[sea_orm(column_name = "发布博客_权限")]
    pub 发_布_博_客_权_限: u8,
}

#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Relation {}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        panic!("No RelationDef")
    }
}

impl ActiveModelBehavior for ActiveModel {}
