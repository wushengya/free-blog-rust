//! SeaORM Entity. Generated by sea-orm-codegen 0.7.0

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Serialize, Deserialize)]
#[sea_orm(table_name = "用户权限表")]
pub struct Model {
    #[sea_orm(column_name = "用户id", primary_key)]
    pub 用_户_id: u32,
    #[sea_orm(column_name = "登录权限")]
    pub 登_录_权_限: u8,
    #[sea_orm(column_name = "评论权限")]
    pub 评_论_权_限: u8,
}

#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Relation {}

impl RelationTrait for Relation {
    fn def(&self) -> RelationDef {
        panic!("No RelationDef")
    }
}

impl ActiveModelBehavior for ActiveModel {}
