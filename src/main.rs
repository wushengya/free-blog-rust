mod tool_bag;
mod app_start;
mod controller;
mod resource;
#[macro_use] extern crate lazy_static;




#[actix_web::main]
async fn main() {
    app_start::app_start().await;
}
