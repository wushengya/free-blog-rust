
pub mod account_manage;

use actix_web::web;



pub fn controller_config(cfg: &mut web::ServiceConfig) {
	cfg.configure(account_manage::controller_config);
}