use crate::{tool_bag::{states,app::middleware::*}, app_start::app_states::{*, state_verification_code::{MVerCodeEmail, EmailCode}}};
use actix_web::{get, web};
use lettre::Address;
use serde::Deserialize;


pub fn controller_config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/cs")
            .wrap(actix_web::middleware::NormalizePath::trim())
            .wrap(ware_example::Example::new(404))
            .service(get_root)
            .service(get_email_code)
    );
}

#[get("")]
async fn get_root(message: web::Data<state_example::Example>) -> &'static str {
    println!("{}", &message.0);
    message.0
}

#[derive(Deserialize)]
struct Info {
    email: String,
}
#[get("/email_code")]
async fn get_email_code(info: web::Query<Info>,code:web::Data<MVerCodeEmail>)  -> String {
    let mut code = code.lock().await;

    let a = code.insert_6(info.email.clone().try_into().unwrap());
    
    println!("{:#?}",&code);    
    a
}
