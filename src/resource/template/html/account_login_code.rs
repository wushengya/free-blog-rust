use serde::Serialize;
use tinytemplate::TinyTemplate;
use tokio::{fs::File, io::AsyncReadExt};



#[derive(Serialize)]	
pub struct AccountLoginCode {
	pub code:String
}
pub async fn account_login_code(t:&AccountLoginCode)->String{
	let mut tt = TinyTemplate::new();
	let mut file = File::open("src/resource/template/html/account_login_code.html").await.unwrap();
	let mut s = String::new();
	file.read_to_string(&mut s).await;
	tt.add_template("code", s.as_str());
	tt.render("code", t).unwrap()
}



#[cfg(test)]
mod tests{
    use super::*;

	#[tokio::test]
	async fn account_login_code_test(){
		let code = AccountLoginCode{
			code:String::from("123456")
		};
		let s = account_login_code(&code).await;
		println!("{}",s);
	}
}