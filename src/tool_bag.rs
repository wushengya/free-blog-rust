pub mod config;
pub mod email;
pub mod mysql;
pub mod app;
pub mod file_piece;

//<< pub-use
pub use self::config::free_config::FREE_CONFIG; //free_config 配置对象
pub use self::config::mysql::MYSQL_DB;//mysql_db
pub use crate::app_start::app_states as states ;//全局状态
pub use crate::tool_bag::email::Email;//Email邮箱
pub use crate::tool_bag::email::EmailBody;//控制Email邮箱发送内容类型枚举
pub use crate::tool_bag::email::EmailBuild;//导入 邮箱功能接口
pub use crate::tool_bag::email::EmailSend;//导入 邮箱功能接口
pub use sea_orm::Condition;//常用SQL条件控制
pub use crate::tool_bag::mysql::table ;//SQL全部对象
pub use crate::tool_bag::mysql::table::prelude as entity_table;//SQL表实体们
pub use crate::tool_bag::mysql::table::sea_orm_active_enums as table_enums;//SQL中的所有枚举们
pub use crate::tool_bag::mysql::mysql业务;//SQL业务,一般是事物的存在
pub use crate::resource::template::html as html_template;//html模板
