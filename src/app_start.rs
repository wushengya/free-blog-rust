mod first_init;
mod app_server;
pub mod app_states;

use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, HttpRequest};





pub(super) async fn app_start() -> Result<(),()>{
	
	//: 优先初始化
	app_start_init().await;
	//: 启动服务
    app_server::init_server().await;
	
	Ok(())
}

async fn app_start_init(){
	//: 初始化 静态变量,它必须第一个初始化,因为内部存在配置文件加载
	first_init::lazy_static_init::init().await;
	//: 其他初始化
	first_init::other_init::init().await;

}
